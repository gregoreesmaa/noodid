package ee.ut.pillime.noodid.db.repository;

import ee.ut.pillime.noodid.db.Pillimees;
import org.springframework.data.repository.CrudRepository;

public interface PillimeesRepository extends CrudRepository<Pillimees, Integer> {
}
